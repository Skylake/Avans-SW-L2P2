package nl.exdev.storedwisely.Util;

import android.Manifest;
import android.location.LocationManager;

public class Constants {

    // Application definitions (permissions).
    public static final String PERMISSION_GPS = Manifest.permission.ACCESS_FINE_LOCATION;
    public static final String PERMISSION_NFC = Manifest.permission.NFC;
    public static final String DEBUGGER_TAG   = "StoredWisely";

    // Application shared preferences (settings).
    public static final String APPLICATION_PREFERENCE_NAME = "StoredWiselyPreferences";

    // Specific GPS definitions.
    public static final String LOCATION_PROVIDER            = LocationManager.GPS_PROVIDER;
    public static final long   LOCATION_MIN_UPDATE_INTERVAL = 5000; // unit: ms.
    public static final float  LOCATION_MIN_UPDATE_DISTANCE = 100;  // unit: m.

    // Database definitions (META).
    public static final int    DATABASE_VERSION         = 2;
    public static final String DATABASE_NAME            = "StoredWiselyDB";
    public static final String TABLE_NAME_BOXES         = "tblBoxes";
    public static final String TABLE_NAME_LOCATIONS     = "tblLocations";

    // Table definition for TABLE_NAME_BOXES.
    public static final String BOX_KEY_ID               = "box_id";
    public static final String BOX_KEY_NAME             = "box_name";
    public static final String BOX_KEY_LOCATION         = "box_location"; // remove in v2.
    public static final String BOX_KEY_LOCATION_ID      = "box_location_id";
    public static final String BOX_KEY_CONTENT          = "box_content";
    public static final String BOX_KEY_DATE             = "date_added";
    public static final String BOX_KEY_COLOR            = "box_color";

    // Table definitions for TABLE_NAME_LOCATIONS.
    public static final String LOCATION_KEY_ID          = "location_id";
    public static final String LOCATION_KEY_NAME        = "location_name";
    public static final String LOCATION_KEY_ADDRESS     = "location_address";
    public static final String LOCATION_KEY_LONGITUDE   = "location_longitude";
    public static final String LOCATION_KEY_LATITUDE    = "location_latitude";
    public static final String LOCATION_KEY_DATE        = "date_added";
}
