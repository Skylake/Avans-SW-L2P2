package nl.exdev.storedwisely.Location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import nl.exdev.storedwisely.Util.Constants;

import static androidx.core.app.ActivityCompat.requestPermissions;
import static androidx.core.content.ContextCompat.checkSelfPermission;

public class AddressListener implements LocationListener {
    // Declaring the LocationManager and LocationListener.
    LocationManager locationManager;
    Location currentBestLocation = null, lastKnownLocation = null;
    Context mContext;

    public AddressListener(Context mContext) {
        Log.d("StoredWisely", "AddressListener()");
        this.mContext = mContext;
    }

    public void setupLocationService() {
        Log.d("StoredWisely", "setupLocationServices()");

        if (locationManager == null) {
            if (checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // Missing permissions; Call a request to ask for GPS permission now.
                requestPermissions((Activity) mContext, new String[] {Constants.PERMISSION_GPS}, 123);
                return;
            }

            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            Location locationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (locationNetwork != null) {
                lastKnownLocation = locationNetwork;
            }
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                lastKnownLocation = locationGPS;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Constants.LOCATION_MIN_UPDATE_INTERVAL, Constants.LOCATION_MIN_UPDATE_DISTANCE, AddressListener.this);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.LOCATION_MIN_UPDATE_INTERVAL, Constants.LOCATION_MIN_UPDATE_DISTANCE, AddressListener.this);
        }
    }

    public Location returnBestLocation() {
        return currentBestLocation;
    }

    // TODO: Also on destroy
    public void stopLocationService() {
        if (locationManager != null) {
            locationManager.removeUpdates(AddressListener.this);
            Log.d("StoredWisely", "AddressListener: stopLocationService()");
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("StoredWisely", "locationListener: onLocationChanged()");

        if (currentBestLocation == null) {
            currentBestLocation = location;
        }

        long timeDelta = location.getTime() - currentBestLocation.getTime();
        Log.d(Constants.DEBUGGER_TAG, "Positioning service changed with accuracy " + location.getAccuracy() + " s different " + (float) timeDelta / 1000);

        if (timeDelta >= 120000) {
            currentBestLocation = location;
            Log.d(Constants.DEBUGGER_TAG, "Positioning service Location changed due to over " + location.getAccuracy() + " s different " + (float) timeDelta / 1000);
        }


        if (currentBestLocation.getAccuracy() >= location.getAccuracy()) {
            currentBestLocation = location;
        }

        String longitude = String.valueOf(currentBestLocation.getLongitude());
        String latitude = String.valueOf(currentBestLocation.getLatitude());

        Log.d("StoredWisely", String.format("locationListener: Current location is %s long, %s lat.", longitude, latitude));
    }

    public String getLocationAddress(Location location) {
        String _Location = "";
        if (location == null) {
            return _Location;
        }

        // Initialize the geocoder and try to locate the coordinates origin address line.
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> listAddresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (listAddresses != null && listAddresses.size() > 0) {
                _Location = listAddresses.get(0).getAddressLine(0);
                Log.d("StoredWisely", String.format("locationListener: Geocoder detects the following address: %s.", _Location));
                Toast.makeText(mContext, String.format("The following address has been detected: %s.", _Location), Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            Toast.makeText(mContext, String.format("Failed to get location: %s", e.getMessage()), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
        return _Location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle bundle) {
        Log.d("StoredWisely", "locationListener: onStatusChanged()");
        Log.d("StoredWisely", String.format("Location provider status code: %s.", status));
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("StoredWisely", "locationListener: onProviderEnabled()");
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("StoredWisely", "locationListener: onProviderDisabled()");
    }
}
