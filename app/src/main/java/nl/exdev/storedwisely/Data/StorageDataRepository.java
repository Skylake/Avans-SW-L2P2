package nl.exdev.storedwisely.Data;

import android.util.Log;

import java.util.ArrayList;

import nl.exdev.storedwisely.Interfaces.RepositoryObserver;
import nl.exdev.storedwisely.Interfaces.Subject;
import nl.exdev.storedwisely.Model.Box;
import nl.exdev.storedwisely.Util.Constants;

public class StorageDataRepository implements Subject {
    // Declare singleton instance and repository observers.
    private static StorageDataRepository INSTANCE = null;
    private ArrayList<RepositoryObserver> mObservers;

    // Declare static action constants to improve readability.
    private static final int ACTION_STORAGE_ADD    = 0;
    private static final int ACTION_STORAGE_UPDATE = 1;
    private static final int ACTION_STORAGE_REMOVE = 2;

    private StorageDataRepository() {
        mObservers = new ArrayList<>();
    }

    // Create a singleton of this repository.
    public static StorageDataRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new StorageDataRepository();
        }
        return INSTANCE;
    }

    @Override
    public void registerObserver(RepositoryObserver repositoryObserver) {
        if (!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }
    }

    @Override
    public void removeObserver(RepositoryObserver repositoryObserver) {
        if (mObservers.contains(repositoryObserver)) {
            mObservers.remove(repositoryObserver);
        }
    }

    @Override
    public void notifyObservers(int ACTION, Box box) {
        Log.d(Constants.DEBUGGER_TAG, String.format("notifyObservers: ACTION: %s, for '%s'.", ACTION, box));
        // NOTE: Database changes are only made if the observer is available.
        for (RepositoryObserver observer: mObservers) {
            switch (ACTION) {
                case ACTION_STORAGE_ADD:
                    observer.onStorageItemAdd(box);
                    break;
                case ACTION_STORAGE_UPDATE:
                    observer.onStorageItemChange(box);
                    break;
                case ACTION_STORAGE_REMOVE:
                    observer.onStorageItemRemove(box);
                    break;
            }
        }
    }

    public void addStorageItem(Box box) {
        notifyObservers(ACTION_STORAGE_ADD, box);
    }

    public void updateStorageItem(Box box) {
        notifyObservers(ACTION_STORAGE_UPDATE, box);
    }

    public void removeStorageItem(Box box) {
        notifyObservers(ACTION_STORAGE_REMOVE, box);
    }
}
