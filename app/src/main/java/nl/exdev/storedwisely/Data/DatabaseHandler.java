package nl.exdev.storedwisely.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nl.exdev.storedwisely.Model.Box;
import nl.exdev.storedwisely.Util.Constants;

public class DatabaseHandler extends SQLiteOpenHelper {

    private final Context context;

    public DatabaseHandler(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
        this.context = context;
    }

    /*
     Creates the local application database on getWritableDatabase and getReadableDatabase calls
     if no instance exists.
     */
    @Override
    public void onCreate(SQLiteDatabase database) {
        String SQL_TABLE_BOXES = "CREATE TABLE " + Constants.TABLE_NAME_BOXES + " ("
                + Constants.BOX_KEY_ID + " INTEGER PRIMARY KEY, "
                + Constants.BOX_KEY_NAME + " TEXT, "
                + Constants.BOX_KEY_LOCATION + " TEXT, " // Use BOX_KEY_LOCATION_ID, INTEGER -> FK
                + Constants.BOX_KEY_CONTENT + " TEXT, "
                + Constants.BOX_KEY_DATE + " TEXT, "
                + Constants.BOX_KEY_COLOR + " INTEGER"
            + ")";

        Log.i(Constants.DEBUGGER_TAG, String.format("onCreate: SQL_TABLE_BOXES -> %s", SQL_TABLE_BOXES));
        database.execSQL(SQL_TABLE_BOXES);

        // TODO: Eventually use a separate location table in the future?
        /* String SQL_TABLE_LOCATIONS = "CREATE TABLE " + Constants.TABLE_NAME_LOCATIONS + " ("
                + Constants.LOCATION_KEY_ID + " INTEGER PRIMARY KEY, "
                + Constants.LOCATION_KEY_NAME + " TEXT, "
                + Constants.LOCATION_KEY_ADDRESS + " TEXT, "
                + Constants.LOCATION_KEY_LONGITUDE + " TEXT, "
                + Constants.LOCATION_KEY_LATITUDE + " TEXT, "
                + Constants.LOCATION_KEY_DATE + " TEXT"
            + " )";

        Log.i(Constants.DEBUGGER_TAG, String.format("onCreate: SQL_TABLE_LOCATIONS -> %s", SQL_TABLE_LOCATIONS));
        database.execSQL(SQL_TABLE_LOCATIONS); */
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int versionBefore, int versionAfter) {
        // Remove existing database tables and call the onCreate to initialize them again.
        database.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME_BOXES);
        // database.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_NAME_LOCATIONS);
        onCreate(database);
    }

    public long addBox(Box box) {
        ContentValues values = new ContentValues();
        values.put(Constants.BOX_KEY_NAME, box.getName());
        values.put(Constants.BOX_KEY_LOCATION, box.getLocation());
        values.put(Constants.BOX_KEY_CONTENT, box.getContents());
        values.put(Constants.BOX_KEY_DATE, new Date().toString());
        values.put(Constants.BOX_KEY_COLOR, box.getColor());

        SQLiteDatabase database = this.getWritableDatabase();
        Long row = database.insert(Constants.TABLE_NAME_BOXES, null, values);
        Log.d(Constants.DEBUGGER_TAG, String.format("addBox: Inserted 'box', %s at row %s.", box, row));
        return row;
    }

    public Box getBox(int id) {
        Box box = new Box();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(Constants.TABLE_NAME_BOXES, new String[] {
           Constants.BOX_KEY_ID, Constants.BOX_KEY_NAME, Constants.BOX_KEY_LOCATION,
           Constants.BOX_KEY_CONTENT, Constants.BOX_KEY_DATE, Constants.BOX_KEY_COLOR},
                Constants.BOX_KEY_ID + " = ?", new String[]{String.valueOf(id)},
                null, null, null);

        if (cursor != null) {
            // if no record was found:
            if (cursor.getCount() <= 0) {
                return box;
            }
            cursor.moveToFirst();

            box.setId(cursor.getInt(cursor.getColumnIndex(Constants.BOX_KEY_ID)));
            box.setName(cursor.getString(cursor.getColumnIndex(Constants.BOX_KEY_NAME)));
            box.setLocation(cursor.getString(cursor.getColumnIndex(Constants.BOX_KEY_LOCATION)));
            box.setContents(cursor.getString(cursor.getColumnIndex(Constants.BOX_KEY_CONTENT)));
            box.setDate(cursor.getString(cursor.getColumnIndex(Constants.BOX_KEY_DATE)));
            box.setColor(cursor.getInt(cursor.getColumnIndex(Constants.BOX_KEY_COLOR)));
        }
        Log.d(Constants.DEBUGGER_TAG, String.format("getBox: Received 'box', %s.", box));
        return box;
    }

    public List<Box> getBoxes() {
        List<Box> boxes = new ArrayList<Box>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(Constants.TABLE_NAME_BOXES, new String[] {
                        Constants.BOX_KEY_ID, Constants.BOX_KEY_NAME, Constants.BOX_KEY_LOCATION,
                        Constants.BOX_KEY_CONTENT, Constants.BOX_KEY_DATE, Constants.BOX_KEY_COLOR},
                null, null, null, null,
                Constants.BOX_KEY_DATE + " DESC");

        if (cursor.moveToFirst()) {
            do {
                Box box = new Box();
                box.setId(cursor.getInt(cursor.getColumnIndex(Constants.BOX_KEY_ID)));
                box.setName(cursor.getString(cursor.getColumnIndex(Constants.BOX_KEY_NAME)));
                box.setLocation(cursor.getString(cursor.getColumnIndex(Constants.BOX_KEY_LOCATION)));
                box.setContents(cursor.getString(cursor.getColumnIndex(Constants.BOX_KEY_CONTENT)));
                box.setDate(cursor.getString(cursor.getColumnIndex(Constants.BOX_KEY_DATE)));
                box.setColor(cursor.getInt(cursor.getColumnIndex(Constants.BOX_KEY_COLOR)));
                boxes.add(box);
            } while (cursor.moveToNext());
        }

        return boxes;
    }

    public int updateBox(Box box) {
        ContentValues values = new ContentValues();
        values.put(Constants.BOX_KEY_NAME, box.getName());
        values.put(Constants.BOX_KEY_LOCATION, box.getLocation());
        values.put(Constants.BOX_KEY_CONTENT, box.getContents());
        values.put(Constants.BOX_KEY_DATE, new Date().toString());
        values.put(Constants.BOX_KEY_COLOR, box.getColor());

        SQLiteDatabase database = this.getWritableDatabase();
        Log.d(Constants.DEBUGGER_TAG, String.format("updateBox: Update 'box', %s.", box));
        return database.update(Constants.TABLE_NAME_BOXES, values,
                Constants.BOX_KEY_ID + " = ?",
                new String[] {String.valueOf(box.getId())});
    }

    // Remove the record of the given id.
    public void deleteBox(int id) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(Constants.TABLE_NAME_BOXES,
                Constants.BOX_KEY_ID + " = ?",
                new String[] {String.valueOf(id)});
        Log.d(Constants.DEBUGGER_TAG, String.format("deleteBox: Removed 'box', %s.", id));
    }

    // Return the count of stored records to use in the dedicated RecyclerViewAdapter.
    public int getCount() {
        String SQL_COUNT = "SELECT * FROM " + Constants.TABLE_NAME_BOXES;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(SQL_COUNT, null);
        return cursor.getCount();
    }

    // Return the next auto increment id (to initialize NFC stickers with new boxes).
    public int getNextAI() {
        String SQL_NEXT_ID = "SELECT MAX(" + Constants.BOX_KEY_ID + ") AS max_id FROM " + Constants.TABLE_NAME_BOXES;
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(SQL_NEXT_ID, null);

        int id = 0;
        if (cursor.moveToFirst())
        {
            do
            {
                id = cursor.getInt(0);
            } while(cursor.moveToNext());
        }
        return id;
    }
}
