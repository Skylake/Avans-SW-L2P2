package nl.exdev.storedwisely.Interfaces;

import nl.exdev.storedwisely.Model.Box;

// Implement interface to manage storage content.
public interface RepositoryObserver {
    void onStorageItemAdd(Box box);
    void onStorageItemChange(Box box);
    void onStorageItemRemove(Box box);
}