package nl.exdev.storedwisely.Interfaces;

import nl.exdev.storedwisely.Model.Box;

// Implement interface to manage observers.
public interface Subject {
    void registerObserver(RepositoryObserver repositoryObserver);
    void removeObserver(RepositoryObserver repositoryObserver);
    void notifyObservers(int ACTION, Box box);
}
