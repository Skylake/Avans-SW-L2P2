package nl.exdev.storedwisely.Ui;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import nl.exdev.storedwisely.Activities.DetailActivity;
import nl.exdev.storedwisely.Model.Box;
import nl.exdev.storedwisely.R;
import nl.exdev.storedwisely.Util.Constants;

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
public class RecyclerViewBoxAdapter extends
        RecyclerView.Adapter<RecyclerViewBoxAdapter.ViewHolder> {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameTextView;
        public TextView locationTextView;
        public Button detailsButton;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameTextView     = itemView.findViewById(R.id.box_name);
            locationTextView = itemView.findViewById(R.id.box_location);
            detailsButton    = itemView.findViewById(R.id.details_button);
        }
    }

    // Store a member variable for the boxes
    private List<Box> mBoxes = new ArrayList<Box>();
    private List<Box> mFullSet = new ArrayList<Box>(); // temporary

    // Pass in the box array into the constructor
    public RecyclerViewBoxAdapter(List<Box> boxes) {
        setAllBoxes(boxes);
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public RecyclerViewBoxAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // inflate the custom layout
        View boxView = inflater.inflate(R.layout.item_box, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(boxView);
        return viewHolder;
    }

    // involves populating data into the item through holder
    @Override
    public void onBindViewHolder(RecyclerViewBoxAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Box box = mBoxes.get(position);

        // set item views based on your views and data model.
        TextView txtName = viewHolder.nameTextView;
        txtName.setText(box.getName());
        TextView txtLocation = viewHolder.locationTextView;
        txtLocation.setText(box.getLocation());
    }

    // return the total count of items in the list
    @Override
    public int getItemCount() {
        return mBoxes.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);

        holder.detailsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Box item = mBoxes.get(position);
                Log.d("StoredWisely", String.format("Clicked on box: %s", item));

                // Use the view context to start the detail view activity.
                Intent intent = new Intent(view.getContext(), DetailActivity.class);
                intent.putExtra("box", item);
                view.getContext().startActivity(intent);
            }
        });
    }

    public void filter(String text) {
        Log.d(Constants.DEBUGGER_TAG, String.format("Inventory Filter: '%s'.", text));
        mBoxes.clear();
        if (text.isEmpty()) {
            mBoxes.addAll(mFullSet);
        } else {
            text = text.toLowerCase();
            for (Box box : mFullSet) {
                if (box.getName().toLowerCase().contains(text) || box.getLocation().toLowerCase().contains(text) || box.getContents().toLowerCase().contains(text)) {
                    mBoxes.add(box);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void setAllBoxes(Collection<Box> boxes) {
        mBoxes.clear();
        mBoxes.addAll(boxes);
        sortBoxes();
        notifyDataSetChanged();
    }

    public void addBox(Box box) {
        mBoxes.add(box);
        mFullSet.add(box);
        int index = mBoxes.indexOf(box);
        notifyItemInserted(index);
        sortBoxes();
        Log.d(Constants.DEBUGGER_TAG, String.format("Notified box with index '%s' it was inserted.", index));
    }

    public void updateBox(Box box) {
        int index = mBoxes.indexOf(box);
        if (index >= 0) {
            mBoxes.set(index, box);
            notifyItemChanged(index, box);
            sortBoxes();
            Log.d(Constants.DEBUGGER_TAG, String.format("Notified box with index '%s' it was changed.", index));
        }
    }

    public void removeBox(Box box) {
        int index = mBoxes.indexOf(box);
        if (index >= 0) {
            mBoxes.remove(index);
            notifyItemRemoved(index);
            sortBoxes();
            Log.d(Constants.DEBUGGER_TAG, String.format("Notified box with index '%s' it was removed.", index));
        }
    }

    // Sort entries on name (a-z).
    private void sortBoxes() {
        Collections.sort(mBoxes, new Comparator<Box>() {
            @Override
            public int compare(Box box1, Box box2) {
                int diff = box1.getName().compareToIgnoreCase(box2.getName());
                if (diff != 0) {
                    notifyItemChanged(mBoxes.indexOf(box1), box1);
                    notifyItemChanged(mBoxes.indexOf(box2), box2);
                }
                return box1.getName().compareToIgnoreCase(box2.getName());
            }
        });
        mFullSet.clear();
        mFullSet.addAll(mBoxes);
    }
}