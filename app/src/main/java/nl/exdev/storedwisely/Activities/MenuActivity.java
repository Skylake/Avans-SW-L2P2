package nl.exdev.storedwisely.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import java.util.Date;

import nl.exdev.storedwisely.NFC.NFCWriteException;
import nl.exdev.storedwisely.NFC.NfcManager;
import nl.exdev.storedwisely.NFC.WriteTagHelper;
import nl.exdev.storedwisely.R;

import static nl.exdev.storedwisely.Util.Constants.APPLICATION_PREFERENCE_NAME;

public class MenuActivity extends AppCompatActivity {
    SharedPreferences sp;

    // Declaring the NFC Adapter.
    NfcManager nfcManager;
    WriteTagHelper writeHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load and decide the color scheme defined in the application shared preferences.
        sp = getSharedPreferences(APPLICATION_PREFERENCE_NAME, 0);
        int colorScheme = sp.getInt("colorScheme", R.style.LightTheme);
        if (colorScheme == R.style.LightTheme) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        setTheme(colorScheme);
        setContentView(R.layout.menu);

        setupNavigation();
        setupNfcServices();
    }

    public void setupNavigation() {
        // Initialize ImageButton onClickListeners with follow-up actions.
        ImageButton inventoryButton = findViewById(R.id.inventory_button);
        inventoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), InventoryActivity.class);
                startActivity(intent);
            }
        });

        ImageButton writeButton = findViewById(R.id.write_button);
        writeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = "Test write of StoredWisely, at: " + new Date().toString();
                Log.d("StoredWisely", String.format("onNewIntent(): Writing %s.", text));
                writeHelper.writeText(text);
            }
        });

        ImageButton themeButton = findViewById(R.id.switch_theme_button);
        themeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Change the value of the application color scheme shared preference and re-launch.
                SharedPreferences.Editor spEditor = sp.edit();
                if (sp.getInt("colorScheme", R.style.LightTheme) == R.style.LightTheme) {
                     spEditor.putInt("colorScheme", R.style.DarkTheme);
                } else {
                     spEditor.putInt("colorScheme", R.style.LightTheme);
                }
                spEditor.commit();

                finish();
                startActivity(new Intent(MenuActivity.this, MenuActivity.this.getClass()));
            }
        });

        ImageButton gpsButton = findViewById(R.id.btn_gps_open);
        gpsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
    }

    // Assign NfcManager and override WriteListeners for NFC tag testing purposes.
    // setOnTagReadListener will try to trigger the detected tag accordingly.
    public void setupNfcServices() {
        nfcManager = new NfcManager(this);
        nfcManager.onActivityCreate();

        nfcManager.setOnTagReadListener(new NfcManager.TagReadListener() {
            @Override
            public void onTagRead(String tagRead) {
                Toast.makeText(MenuActivity.this, "Tag detected: " + tagRead, Toast.LENGTH_LONG).show();
                /*
                 * When a readable tag is detected, open the related detail view.
                 * This requires the InventoryActivity to enable changes due to the need of the observer.
                 * Thus passing a bundle to the intent and trigger a DetailActivity afterward.
                 */
                Intent intent = new Intent(getApplicationContext(), InventoryActivity.class);
                intent.putExtra("NFC_TAG", tagRead);
                startActivity(intent);
            }
        });

        writeHelper = new WriteTagHelper(this, nfcManager);
		// Use the tag writer helper class and overrule the default results.
		nfcManager.setOnTagWriteListener(new NfcManager.TagWriteListener() {
			@Override
			public void onTagWritten() {
                writeHelper.dialog.dismiss();
				Toast.makeText(MenuActivity.this, "Tag seems to be working correctly!", Toast.LENGTH_LONG).show();
			}
		});
		// Display exception and notify the user the given tag is incompatible.
		nfcManager.setOnTagWriteErrorListener(new NfcManager.TagWriteErrorListener() {
			@Override
			public void onTagWriteError(NFCWriteException exception) {
                writeHelper.dialog.dismiss();
				Toast.makeText(MenuActivity.this, String.format("Tag seems to be incompatible, reason: %s.", exception.getType().toString()), Toast.LENGTH_LONG).show();
			}
		});
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("StoredWisely", "onNewIntent()");
        nfcManager.onActivityNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcManager.onActivityResume();
    }

    @Override
    protected void onPause() {
        nfcManager.onActivityPause();
        super.onPause();
    }
}
