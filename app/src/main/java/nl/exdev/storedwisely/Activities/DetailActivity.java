package nl.exdev.storedwisely.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;

import com.pes.androidmaterialcolorpickerdialog.ColorPicker;
import com.pes.androidmaterialcolorpickerdialog.ColorPickerCallback;

import java.util.Date;

import nl.exdev.storedwisely.Data.StorageDataRepository;
import nl.exdev.storedwisely.Location.AddressListener;
import nl.exdev.storedwisely.Model.Box;
import nl.exdev.storedwisely.NFC.NfcManager;
import nl.exdev.storedwisely.NFC.WriteTagHelper;
import nl.exdev.storedwisely.R;
import nl.exdev.storedwisely.Util.Constants;

public class DetailActivity extends AppCompatActivity {

    // Declaring the tag manager and writer (NFC).
    private NfcManager nfcManager;
    private WriteTagHelper writeHelper;

    // Declaring the address listener (GPS).
    private AddressListener addressListener;
    private Box box;
    private int nextBoxId = 0;

    EditText txtName;
    EditText txtLocation;
    EditText txtItems;
    TextView txtLastEdit;
    ImageView viewColor;
    Button btnGps;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_box);

        // Initialize the tag manager and writer for NFC support.
        nfcManager = new NfcManager(this);
        nfcManager.onActivityCreate();

        nfcManager.setOnTagReadListener(new NfcManager.TagReadListener() {
            @Override
            public void onTagRead(String tagRead) {
                // Don't do anything if read without NFC write pop-up.
                return;
            }
        });

        writeHelper = new WriteTagHelper(this, nfcManager);
        nfcManager.setOnTagWriteErrorListener(writeHelper);
        nfcManager.setOnTagWriteListener(writeHelper);

        // Initialize the address listener which is required later.
        addressListener = new AddressListener(this);
        addressListener.setupLocationService();

        txtName     = findViewById(R.id.txtName);
        txtLocation = findViewById(R.id.txtLocation);
        txtItems    = findViewById(R.id.txtItems);
        txtLastEdit = findViewById(R.id.textLastUpdate);
        viewColor   = findViewById(R.id.viewColor);
        btnGps      = findViewById(R.id.btn_gps_location);
        btnSave     = findViewById(R.id.btnSave);

        Bundle bundle = getIntent().getExtras();
        if (bundle.getParcelable("box") != null) {
                box = bundle.getParcelable("box");
                txtName.setText(box.getName());
                txtLocation.setText(box.getLocation());
                txtItems.setText(box.getContents());
                txtLastEdit.setText(box.getDate());
                viewColor.setBackgroundColor(box.getColor());
        } else {
            nextBoxId = bundle.getInt("box_next_id");
            Log.d(Constants.DEBUGGER_TAG, String.format("DetailActivity(): nextBoxId '%s'.", nextBoxId));
            box = new Box();
            box.setDate(new Date().toString());
            txtLastEdit.setText(box.getDate());
        }

        // Initialize the color picker with a default visible color.
        final ColorPicker cp = new ColorPicker(DetailActivity.this, 88, 156, 230);
        viewColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Show color picker dialog
                cp.show();

                // Set a new Listener called when user click "select"
                cp.setCallback(new ColorPickerCallback() {
                    @Override
                    public void onColorChosen(@ColorInt int color) {
                        // Dismiss the dialog after color was chosen
                        cp.dismiss();

                        // Assign the new color to the box entity.
                        box.setColor(color);
                        viewColor.setBackgroundColor(color);
                    }
                });
            }
        });

        btnGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Fetch the best location if available.
                String detectedAddress = addressListener.getLocationAddress(addressListener.returnBestLocation());

                // If the detected address is empty, display a notification to wait / enable gps.
                if (detectedAddress.isEmpty()) {
                    Toast.makeText(view.getContext(), String.format("Unable to detect your location, verify your location sensor is enabled or wait for some time and try again."), Toast.LENGTH_SHORT).show();
                    return;
                }

                // Otherwise assign the location variable of the box object and the view.
                box.setLocation(detectedAddress);
                txtLocation.setText(box.getLocation());
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Stop fetching address notifications on submit.
                // eventually just add a separate Location object instead.
                addressListener.stopLocationService();
                nfcManager.onActivityPause();

                // Set the new values to the box object followed by an insert / update.
                box.setName(txtName.getText().toString());
                box.setLocation(txtLocation.getText().toString());
                box.setContents(txtItems.getText().toString());
                box.setDate(new Date().toString());

                // Check if it's an update or insert using by validating the value of box.getId().
                if (box.getId() == 0) {
                    Log.d(Constants.DEBUGGER_TAG, String.format("onClick(): addStorageItem '%s'.", box));
                    StorageDataRepository.getInstance().addStorageItem(box);
                } else {
                    Log.d(Constants.DEBUGGER_TAG, String.format("onClick(): updateStorageItem '%s'.", box));
                    StorageDataRepository.getInstance().updateStorageItem(box);
                }

                // Display a toast message after the box was successfully added / updated and end the activity.
                Toast.makeText(view.getContext(), String.format("The changes to %s have been saved successfully.", box.getName()), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("StoredWisely", "onNewIntent()");
        nfcManager.onActivityNewIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Override the activity menu to contain search functionality.
        getMenuInflater().inflate(R.menu.details, menu);

        final MenuItem attachNfcItem = menu.findItem(R.id.action_attach_nfc);
        attachNfcItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Log.d(Constants.DEBUGGER_TAG, String.format("onClick(): attachNfcTag '%s'.", box));

                if (box.getId() != 0) {
                    // Write box.id to the NFC tag to attach the open box to it.
                    writeHelper.writeText(String.format("%s", box.getId()));
                    Log.d("StoredWisely", String.format("onClick(): Writing %s.", box.getId()));
                } else {
                    // Receive the next box.id from the databaseHandler and initialize the sticker.
                    writeHelper.writeText(String.format("%s", nextBoxId));
                    Log.d("StoredWisely", String.format("onClick(): Writing %s.", nextBoxId));
                }
                return false;
            }
        });

        final MenuItem deleteItem = menu.findItem(R.id.action_delete);
        deleteItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Log.d(Constants.DEBUGGER_TAG, String.format("onClick(): removeStorageItem '%s'.", box));
                StorageDataRepository.getInstance().removeStorageItem(box);
                finish();
                return false;
            }
        });
        return true;
    }

    @Override
    protected void onPause() {
        addressListener.stopLocationService();
        nfcManager.onActivityPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        addressListener.setupLocationService();
        nfcManager.onActivityResume();
    }

    @Override
    protected void onDestroy() {
        addressListener.stopLocationService();
        super.onDestroy();
    }
}
