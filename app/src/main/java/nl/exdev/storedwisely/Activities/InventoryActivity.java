package nl.exdev.storedwisely.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import nl.exdev.storedwisely.Data.DatabaseHandler;
import nl.exdev.storedwisely.Data.StorageDataRepository;
import nl.exdev.storedwisely.Interfaces.RepositoryObserver;
import nl.exdev.storedwisely.Interfaces.Subject;
import nl.exdev.storedwisely.Model.Box;
import nl.exdev.storedwisely.NFC.NfcManager;
import nl.exdev.storedwisely.R;
import nl.exdev.storedwisely.Ui.RecyclerViewBoxAdapter;
import nl.exdev.storedwisely.Util.Constants;

public class InventoryActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, RepositoryObserver {

    // Declaring the recycler, adapter, observer and database handler.
    private RecyclerView recyclerView;
    private RecyclerViewBoxAdapter recyclerViewBoxAdapter;
    private Subject mStorageDataRepository;
    private DatabaseHandler databaseHandler;

    // Declaring the tag manager (NFC).
    private NfcManager nfcManager;

    // Declare the AdView.
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory);

        // Initialize database handler to receive contents.
        databaseHandler = new DatabaseHandler(this);

        // Get the RecyclerView from the view and change properties.
        recyclerView = findViewById(R.id.rvBoxes);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // Followed by filling it with the contents of the database.
        List<Box> boxes = databaseHandler.getBoxes();
        Log.d(Constants.DEBUGGER_TAG, String.format("onCreate(): Loaded '%s' database box results.", boxes.size()));
        if (boxes.size() == 0) {
            Toast.makeText(getApplicationContext(), "No items available yet, try to add a new entry first.", Toast.LENGTH_SHORT).show();
        }

        // attach the adapter to the RecyclerView to populate items
        recyclerViewBoxAdapter = new RecyclerViewBoxAdapter(boxes);
        recyclerView.setAdapter(recyclerViewBoxAdapter);
        recyclerViewBoxAdapter.notifyDataSetChanged();

        // register observer to get notifications when changes are made.
        mStorageDataRepository = StorageDataRepository.getInstance();
        mStorageDataRepository.registerObserver(this);

        FloatingActionButton btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                intent.putExtra("box_next_id", databaseHandler.getNextAI());
                startActivity(intent);
            }
        });

        // If this Activity was started with an extra NFC_TAG argument, trigger DetailActivity.
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String NfcData = bundle.getString("NFC_TAG");
            processNFC(NfcData);
        }

        // Initialize the tag manager and writer for NFC support.
        nfcManager = new NfcManager(this);
        nfcManager.onActivityCreate();

        // Implement NFC scanning functionality to open the associated box.
        nfcManager.setOnTagReadListener(new NfcManager.TagReadListener() {
            @Override
            public void onTagRead(String tagRead) {
                Toast.makeText(InventoryActivity.this, "tag read:" + tagRead, Toast.LENGTH_LONG).show();
                processNFC(tagRead);
            }
        });

        // Initialize the MobileAds SDK.
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        // Request an advertisement and assign it to the adView.
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("D11352E92B98E8243622F6E19090DD99").build();
        mAdView.loadAd(adRequest);
        // Eventually implement mAdView.setAdListener() to override default functionality.
    }

    // Process NFC request information.
    private void processNFC(String NfcData) {
        try {
            int boxId = Integer.parseInt(NfcData);
            Log.d("StoredWisely", String.format("Loading box using NFC id: %s", boxId));
            Box box = new Box();
            if (boxId != 0) {
                // Load the box from database if a valid number is assigned.
                box = databaseHandler.getBox(boxId);
            }
            // Open the existing entry using the detected NFC data or use it for a new entry.
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            if (box.getId() != 0) {
                intent.putExtra("box", box);
            } else {
                intent.putExtra("box_next_id", databaseHandler.getNextAI());
                Toast.makeText(getApplicationContext(), String.format("Unable to find related information, adding a new entry."), Toast.LENGTH_LONG).show();
            }
            startActivity(intent);
        } catch (NumberFormatException nfe) {
            // If the NFC data gives a format exception, use it for a new entry.
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra("box_next_id", databaseHandler.getNextAI());
            Toast.makeText(getApplicationContext(), String.format("Unable to find related information, adding a new item."), Toast.LENGTH_LONG).show();
            startActivity(intent);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("StoredWisely", "onNewIntent()");
        nfcManager.onActivityNewIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Override the activity menu to contain search functionality.
        getMenuInflater().inflate(R.menu.inventory, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // Filter content on submit.
        recyclerViewBoxAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // Filter content on change.
        recyclerViewBoxAdapter.filter(newText);
        return false;
    }

    @Override
    public void onStorageItemAdd(Box box) {
        // add the box and append it to the adapter.
        box.setId((int) databaseHandler.addBox(box));
        recyclerViewBoxAdapter.addBox(box);
    }

    @Override
    public void onStorageItemChange(Box box) {
        // update the database record and the adapter item.
        databaseHandler.updateBox(box);
        recyclerViewBoxAdapter.updateBox(box);
    }

    @Override
    public void onStorageItemRemove(Box box) {
        // remove the item from the database and adapter.
        databaseHandler.deleteBox(box.getId());
        recyclerViewBoxAdapter.removeBox(box);
    }

    @Override
    protected void onPause() {
        nfcManager.onActivityPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcManager.onActivityResume();
    }

    @Override
    protected void onDestroy() {
        mStorageDataRepository.removeObserver(this);
        super.onDestroy();
    }
}
