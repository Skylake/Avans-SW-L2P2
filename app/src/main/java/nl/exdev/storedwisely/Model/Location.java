package nl.exdev.storedwisely.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Location implements Parcelable {

    private int location_id;
    private String location_name;
    private String location_address;
    private String location_longitude;
    private String location_latitude;
    private String date_added;

    public Location() {

    }

    public Location(int location_id, String location_name, String location_address, String location_longitude, String location_latitude, String date_added) {
        this.location_id = location_id;
        this.location_name = location_name;
        this.location_address = location_address;
        this.location_longitude = location_longitude;
        this.location_latitude = location_latitude;
        this.date_added = date_added;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public String getLocation_longitude() {
        return location_longitude;
    }

    public void setLocation_longitude(String location_longitude) {
        this.location_longitude = location_longitude;
    }

    public String getLocation_latitude() {
        return location_latitude;
    }

    public void setLocation_latitude(String location_latitude) {
        this.location_latitude = location_latitude;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Location)) return false;

        Location location = (Location) o;

        return this.location_id == location.location_id;
    }

    @Override
    public String toString() {
        return "Location{" +
                "location_id=" + location_id +
                ", location_name='" + location_name + '\'' +
                ", location_address='" + location_address + '\'' +
                ", location_longitude='" + location_longitude + '\'' +
                ", location_latitude='" + location_latitude + '\'' +
                ", date_added='" + date_added + '\'' +
                '}';
    }

    protected Location(Parcel in) {
        location_id = in.readInt();
        location_name = in.readString();
        location_address = in.readString();
        location_longitude = in.readString();
        location_latitude = in.readString();
        date_added = in.readString();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) { return new Location(in); }

        @Override
        public Location[] newArray(int size) { return new Location[size]; }
    };

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(location_id);
        dest.writeString(location_name);
        dest.writeString(location_address);
        dest.writeString(location_longitude);
        dest.writeString(location_latitude);
        dest.writeString(date_added);
    }
}