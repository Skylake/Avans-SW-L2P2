package nl.exdev.storedwisely.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Box implements Parcelable {

    private int id;
    private String name;
    private String location;
    // private int location_id; // todo: add.
    private String contents;
    private String date;
    private int color;

    public Box() {

    }

    public Box(int id, String name, String location, String contents, String date, int color) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.contents = contents;
        this.date = date;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Box)) return false;

        Box box = (Box) o;

        return this.id == box.id;
    }

    @Override
    public String toString() {
        return "Box{" +
                "id='" + id+ '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", contents='" + contents + '\'' +
                ", date='" + date + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    protected Box(Parcel in) {
        id = in.readInt();
        name = in.readString();
        location = in.readString();
        contents = in.readString();
        date = in.readString();
        color = in.readInt();
    }

    public static final Creator<Box> CREATOR = new Creator<Box>() {
        @Override
        public Box createFromParcel(Parcel in) { return new Box(in); }

        @Override
        public Box[] newArray(int size) { return new Box[size]; }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(location);
        dest.writeString(contents);
        dest.writeString(date);
        dest.writeInt(color);
    }
}
