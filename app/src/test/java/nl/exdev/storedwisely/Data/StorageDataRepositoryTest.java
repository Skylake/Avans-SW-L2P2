package nl.exdev.storedwisely.Data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;

import nl.exdev.storedwisely.Activities.InventoryActivity;
import nl.exdev.storedwisely.Interfaces.RepositoryObserver;
import nl.exdev.storedwisely.Model.Box;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class StorageDataRepositoryTest {
    private ArrayList<RepositoryObserver> mObservers;
    private ArrayList<Box> storageBoxes;

    @BeforeEach
    void setUp() {
        mObservers = new ArrayList<>();
        storageBoxes = new ArrayList<>();
    }

    @Disabled
    @DisplayName("Register a new Observer to the mObservers list.")
    void registerObserver_forObserver_expectObserverAdded() {
        // Arrange
        InventoryActivity inventoryActivity = new InventoryActivity();
        RepositoryObserver repositoryObserver = mock(RepositoryObserver.class);

        // Act
        if (!mObservers.contains(repositoryObserver)) {
            mObservers.add(repositoryObserver);
        }

        // Assert
        assertEquals(1, mObservers.size());
        assertEquals(RepositoryObserver.class, mObservers.get(0));
    }

    @Disabled
    @DisplayName("Remove a Observer from the mObservers list.")
    void removeObserver_forObserver_expectObserverRemoved() {
        // Arrange
        RepositoryObserver repositoryObserver = mock(RepositoryObserver.class);

        // Act
        mObservers.add(repositoryObserver);
        if (mObservers.contains(repositoryObserver)) {
            mObservers.remove(repositoryObserver);
        }

        // Assert
        assertEquals(0, mObservers.size());
    }

    @Test
    @DisplayName("Add a storage item to the storageBoxes.")
    void addStorageItem_forBox_expectBoxAdded() {
        // Arrange
        Box boxO = new Box(1, "Box 1", "Oudenbosch", "Contents of the box.", new Date().toString(), 0);

        // Act
        storageBoxes.add(boxO);

        // Assert
        assertEquals(1, storageBoxes.size());
    }

    @Test
    @DisplayName("Update a storage item of the storageBoxes.")
    void updateStorageItem_forBox_expectBoxUpdated() {
        // Arrange
        Box boxO = new Box(1, "Box 1", "Oudenbosch", "Contents of the box.", new Date().toString(), 0);
        Box boxE = new Box(2, "Box 2", "Etten-Leur", "Some other stuff.", new Date().toString(), 0);

        // Act
        storageBoxes.add(boxO);
        storageBoxes.add(boxE);

        int index = storageBoxes.indexOf(boxE);
        if (index >= 0) {
            boxE.setContents("New contents here.");
            boxE.setDate(new Date().toString());
            storageBoxes.set(index, boxE);
        }

        // Assert
        assertEquals(2, storageBoxes.size());
        assertEquals("New contents here.", storageBoxes.get(1).getContents());
    }

    @Test
    @DisplayName("Remove a storage item of the storageBoxes.")
    void removeStorageItem_forBox_expectBoxRemoved() {
        // Arrange
        Box box = new Box(1, "Box 1", "Oudenbosch", "Contents of the box", new Date().toString(), 0);

        // Act
        storageBoxes.add(box);
        int index = storageBoxes.indexOf(box);
        if (index >= 0) {
            storageBoxes.remove(index);
        }

        // Assert
        assertEquals(0, storageBoxes.size());
    }
}