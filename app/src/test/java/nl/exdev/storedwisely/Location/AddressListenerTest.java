package nl.exdev.storedwisely.Location;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AddressListenerTest {

    // Declaring the targetLocation.
    Location targetLocation;
    Geocoder geocoder;
    List<Address> addressList;
    double locLatitude;
    double locLongitude;

    @Disabled
    @BeforeEach
    void setUp() {
        locLatitude = 51.590771;
        locLongitude = 4.5419535;

        // Setup a fixed point and call getLocationAddress to test what returns.
        Location targetLocation = mock(Location.class);
        when(targetLocation.getLatitude()).thenReturn(locLatitude);
        when(targetLocation.getLongitude()).thenReturn(locLongitude);
        this.targetLocation = targetLocation;

        // Set up the geocoder and an address to return if the coordinates are correct.
        Geocoder geocoder = mock(Geocoder.class);
        Address address = new Address(Locale.getDefault()); // mock(Address.class);
        address.setAddressLine(0, "Luit. Looymanshof 1, 4731GX, Netherlands") ;
        this.addressList = (List<Address>) address;
        this.geocoder = geocoder;
    }

    @Disabled
    @Test
    void getLocationAddress() {
        // Arrange
        String _Location = "";

        if (targetLocation.getLatitude() == 0 || targetLocation.getLongitude() == 0) {
            // Assert
            assertEquals(0, targetLocation.getLatitude());
            assertEquals(0, targetLocation.getLongitude());
            assertEquals("", _Location);
            return;
        }

        // Act
        try {
            when(geocoder.getFromLocation(targetLocation.getLatitude(), targetLocation.getLongitude(), 1)).thenReturn(addressList);
            List<Address> listAddresses = geocoder.getFromLocation(targetLocation.getLatitude(), targetLocation.getLongitude(), 1);


            if (listAddresses != null && listAddresses.size() > 0) {
                _Location = listAddresses.get(0).getAddressLine(0);
            }
        } catch (Exception IOException) {
            assertEquals(0, targetLocation.getLatitude());
            assertEquals(0, targetLocation.getLongitude());
            assertEquals("", _Location);
            return;
        }

        // Assert
        assertEquals(51.590771, targetLocation.getLatitude());
        assertEquals(4.5419535, targetLocation.getLongitude());
        assertEquals("test", _Location);
    }
}