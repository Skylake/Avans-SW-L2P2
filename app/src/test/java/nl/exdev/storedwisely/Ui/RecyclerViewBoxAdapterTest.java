package nl.exdev.storedwisely.Ui;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import nl.exdev.storedwisely.Model.Box;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class RecyclerViewBoxAdapterTest {

    private List<Box> mBoxes;
    private List<Box> mFullSet;

    @BeforeEach
    void SetUp() {
        mBoxes = new ArrayList<>();
        mFullSet = new ArrayList<>();
        setAllBoxes_foxBoxes_expectAddedAndSortedForNonFull();
    }

    @Test
    @DisplayName("Set two copies of all boxes to mBoxes (sorted) and mFullSet.")
    void setAllBoxes_foxBoxes_expectAddedAndSortedForNonFull() {
        // Arrange
        List<Box> boxes = new ArrayList<>();
        boxes.add(new Box(1, "Box parts", "Oudenbosch", "Contents of the box.", "Mon Jan 14 12:18:00 CET 2019", 0));
        boxes.add(new Box(2, "Box 2", "Etten-Leur", "Some other stuff.", new Date().toString(), 0));
        boxes.add(new Box(3, "Box Test", "Breda", "I'm so done with this.", "Sat Jan 11 11:18:00 CET 2020", 0));

        // Act
        mBoxes.clear();
        mBoxes.addAll(boxes);

        // Sort entries of mBoxes on name (a-z, expects id:2,1,3).
        Collections.sort(mBoxes, new Comparator<Box>() {
            @Override
            public int compare(Box box1, Box box2) {
                return box1.getName().compareToIgnoreCase(box2.getName());
            }
        });

        mFullSet.clear();
        mFullSet.addAll(boxes);

        // Assert
        assertNotEquals(mBoxes, mFullSet);
        assertEquals(mBoxes.size(), mFullSet.size());
    }

    @Test
    @DisplayName("Filter the storage items with a given argument.")
    void filter_forBoxes_expectFiltered() {
        // Arrange
        String filterArg = "test";

        // Act
        mBoxes.clear();
        if (filterArg.isEmpty()) {
            mBoxes.addAll(mFullSet);
        } else {
            filterArg = filterArg.toLowerCase();
            for (Box box : mFullSet) {
                if (box.getName().toLowerCase().contains(filterArg)
                        || box.getLocation().toLowerCase().contains(filterArg)
                        || box.getContents().toLowerCase().contains(filterArg)) {
                    mBoxes.add(box);
                }
            }
        }

        // Assert
        assertNotEquals(mBoxes, mFullSet);
        assertEquals(1, mBoxes.size());
        assertEquals("Box Test", mBoxes.get(0).getName());
    }
}